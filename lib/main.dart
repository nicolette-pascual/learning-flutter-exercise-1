import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'generated/i18n.dart';
import 'logData.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //FIXME: put strings in i18n. Study on how it's used not just install it.
    return MaterialApp(
      localizationsDelegates: [S.delegate],
      supportedLocales: S.delegate.supportedLocales,
      title: 'Work Logger',
      home: App(),
    );
  }
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text(S.of(context).work_logger)),
      ),
      body: MyCustomForm(),
    );
  }
}

class MyCustomForm extends StatefulWidget {
  @override
  _MyCustomFormState createState() => _MyCustomFormState();
}

class _MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();
  var _dropDownProjects = ["Hydra", "Dragon MY", "Dragon P2P"];
  var _currentItemSelected = 'Hydra';
  static const int entries = 3;
  static final RegExp durRegExp = new RegExp("[\\,|\\_|\\-]");
  List<LogData> logsData = List<LogData>();
  List<LogData> displayList = List<LogData>();
  TextEditingController durationController = TextEditingController();
  TextEditingController remarkController = TextEditingController();
  double totalDuration = 0.0;
  int start = 0;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            //ROW 1 - New Logs Section
            _logFormSection(),
            //ROW 2 - Logs Section
            _logTitleSection(),
            //ROW 3 - Logs Data
            _logDataSection(),
            //ROW 4 - show more button
            _showMoreButton()
          ],
        ),
      ),
    );
  }

  Widget _logFormSection() {
    return Row(
      children: <Widget>[
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    S.of(context).new_log,
                    style: ThemeText.title,
                  )),
              TextFormField(
                controller: durationController,
                decoration: InputDecoration(labelText: S.of(context).duration),
                keyboardType: TextInputType.number,
                inputFormatters: [
                  new BlacklistingTextInputFormatter(
                      //FIXME: regex in constant
                      durRegExp),
                ],
                validator: (value) {
                  try {
                    double.parse(value);
                  } catch (e) {
                    Scaffold.of(context).showSnackBar(SnackBar(
                      content: Text(S.of(context).invalid_duration_input),
                    ));
                    throw (e);
                  }
                  if (value.isEmpty) {
                    return S.of(context).please_enter_duration;
                  }
                  return null;
                },
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Text(S.of(context).project),
              ),
              DropdownButton<String>(
                isExpanded: true,
                items: _dropDownProjects.map((String dropdownStringItem) {
                  return DropdownMenuItem<String>(
                    value: dropdownStringItem,
                    child: Text(dropdownStringItem),
                  );
                }).toList(),
                onChanged: (String selected) {
                  _onDropDownItemSelected(selected);
                },
                value: _currentItemSelected,
              ),
              TextFormField(
                controller: remarkController,
                decoration: InputDecoration(labelText: S.of(context).remark),
                validator: (value) {
                  if (value.isEmpty) {
                    return S.of(context).please_enter_some_remark;
                  }
                  return null;
                },
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: RaisedButton(
                  onPressed: () {
                    _onAddPressed();
                  },
                  child: Text(S.of(context).add),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _logTitleSection() {
    return Row(
      children: <Widget>[
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Text(
                S.of(context).logs,
                style: ThemeText.title,
              ),
              Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: Text(
                      //FIXME: put in i18n, check on how to add variables within phrases
                      S
                          .of(context)
                          .total_duration(totalDuration.toStringAsFixed(1)))),
            ],
          ),
        ),
      ],
    );
  }

  Widget _logDataSection() {
    return Row(
      children: <Widget>[
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Flexible(
                child: _getList(),
              )
            ],
          ),
        ),
      ],
    );
  }

  Widget _showMoreButton() {
    return Visibility(
      visible: logsData.length == displayList.length ? false : true,
      child: Row(
        children: <Widget>[
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                FlatButton(
                  color: Colors.blue,
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  onPressed: () {
                    _onShowMorePressed();
                  },
                  child: Text('show more'),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _getList() {
    _makeList();
    ListView myList = new ListView.builder(
        physics: const ScrollPhysics(),
        shrinkWrap: true,
        itemCount: displayList.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(
                '${displayList[index].getDuration().toStringAsFixed(1)}h - ${displayList[index].getProject()}'),
            subtitle: Text('${displayList[index].getRemark()}'),
            trailing: Column(
              children: <Widget>[
                Container(
                  child: IconButton(
                    icon: Icon(Icons.delete),
                    onPressed: () {
                      _onDeleteIconPressed(index);
                    },
                  ),
                )
              ],
            ),
          );
        });
    return myList;
  }

  void _onAddPressed() {
    if (_formKey.currentState.validate()) {
      setState(() {
        logsData.add(new LogData(double.parse(durationController.text),
            _currentItemSelected, remarkController.text));
        totalDuration += double.parse(durationController.text);

        //FIXME: 3 as constant.
        //FIXME: unnecessary computation, not elegant solution
        start = logsData.length - entries;
      });
    }
  }

  void _onDeleteIconPressed(int index) {
    setState(() {
      int diff = (logsData.length - 1) - index;
      totalDuration -= displayList[index].getDuration();
      displayList.removeAt(index);
      logsData.removeAt(diff);

      //FIXME: are these computations really needed? note that widgets loads the list data every build, hence they are updated with the reduced list.
      //TODO: CLUE: study ListView's itemBuilder.
      if ((start - 1) >= 0) {
        start -= 1;
      }
    });
  }

  void _onDropDownItemSelected(String selected) {
    setState(() {
      this._currentItemSelected = selected;
    });
  }

  void _onShowMorePressed() {
    setState(() {
      //FIXME: 3 in constant. imagine needing to change the specs from 3 to 5, you'll need to update all these lines.
      //FIXME: solution isn't elegant
      if ((start - entries) >= 0) {
        start -= entries;
      } else {
        start = 0;
      }
    });
  }

  void _makeList() {
    displayList.clear();
    if (logsData.length <= entries) {
      displayList.addAll(logsData.getRange(0, logsData.length));
    } else {
      displayList.addAll(logsData.getRange(start, logsData.length));
    }
    displayList = displayList.reversed.toList();
  }
}

abstract class ThemeText {
  static const TextStyle title =
      TextStyle(fontWeight: FontWeight.bold, fontSize: 17.0);
}
