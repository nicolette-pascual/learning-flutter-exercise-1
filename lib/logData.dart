class LogData {
  double duration;
  String remark, project;
  LogData(double duration, String project, String remark) {
    this.duration = duration;
    this.project = project;
    this.remark = remark;
  }
  double getDuration() {
    return duration;
  }

  String getProject() {
    return project;
  }

  String getRemark() {
    return remark;
  }
}
